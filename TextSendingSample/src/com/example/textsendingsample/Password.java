package com.example.textsendingsample;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Password extends Activity {
	Button b, sn;
	EditText e;
	String password;
	String defualtPassword;
	int count = 60;
	Timer T;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.activity_password);
		e = (EditText) findViewById(R.id.password);
		b = (Button) findViewById(R.id.dialogButtonOK);
		sn = (Button) findViewById(R.id.sendNow);
		
		setResult(RESULT_OK);//default
		
		/***** Put a password you like *****/
		defualtPassword = "lol";
		/***** Put a password you like *****/
		

		T = new Timer();
		T.scheduleAtFixedRate(new TimerTask() {
			/********
			 * Without looking at the code below
			 * write your own "run" method
			 * ***********/
			@Override
			public void run() {
				
			}
			
//			@Override
//			public void run() {
//				runOnUiThread(new Runnable() {
//					public void run() {
//						Toast.makeText(getBaseContext(), count + " s",
//								Toast.LENGTH_SHORT).show();
//						count -= 10;
//						if (count <= -10) {
//							setResult(RESULT_OK);	
//							T.cancel();
//							finish();
//						}
//					}
//				});
//			}
		}, 0, 10 * 1000);
		
		/********
		 * Without looking at the code below
		 * write your own "run" method
		 * ***********/

		b.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				password = e.getText().toString();
				// if clock within 60 then time scheduler
				if (password.equals(defualtPassword)) {
					setResult(RESULT_CANCELED);
					T.cancel();
					finish();
				} else {
					Toast.makeText(getBaseContext(),
							"Wrong Password, please enter agian",
							Toast.LENGTH_SHORT).show();
				}
			}

		});

		sn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				setResult(RESULT_OK);
				T.cancel();
				finish();
			}

		});
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.i("Password", "On Destroy");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.i("Password", "On Destroy");
	}

	@Override
	public void onDestroy() {
		/*****
		 * Don't forget to call T.cancel() here!
		 * *******/
		T.cancel();
		Log.i("Password", "OnDestroy");
		super.onDestroy();
	}

}