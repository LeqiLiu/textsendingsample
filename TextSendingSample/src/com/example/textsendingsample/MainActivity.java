package com.example.textsendingsample;

import java.util.ArrayList;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/********
 * Great Thanks to:
 * http://mobiforge.com/design-development/sms-messaging-android
 * ***********/

public class MainActivity extends ActionBarActivity {
	Button btnSendSMS;
	EditText txtPhoneNo;
	EditText txtMessage;
	String phoneNo;
	String message;
	String SENT = "SMS_SENT";
	String DELIVERED = "SMS_DELIVERED";
	private BroadcastReceiver sendBroadcastReceiver;
	private BroadcastReceiver deliveryBroadcastReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btnSendSMS = (Button) findViewById(R.id.btnSendSMS);
		txtPhoneNo = (EditText) findViewById(R.id.txtPhoneNo);
		txtMessage = (EditText) findViewById(R.id.txtMessage);

		
		btnSendSMS.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				phoneNo = txtPhoneNo.getText().toString();
				message = txtMessage.getText().toString();
				Log.i("phoneNo.", phoneNo);
				Log.i("Message.", message);

				if (phoneNo.length() > 0 && message.length() > 0) {
					Intent i = new Intent(MainActivity.this, Password.class);
					i.putExtra("Message", message);
					startActivityForResult(i, 0);
				} else {
					Toast.makeText(
							getBaseContext(),
							"Please enter emergency contact and message you want to send",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) {
			
			if (resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(getBaseContext(),
						"Cancelled your text sending!", Toast.LENGTH_SHORT)
						.show();
			} else if (resultCode == Activity.RESULT_OK) {
				sendSMS(phoneNo, message);
			}
		}
	}

	private void sendSMS(String phoneNumber, String message) {
		/*********
		 * Write your own sms
		 * Don't forget to comment out 
		 * Line 141 and line 142  after you finish writing this part...
		 * **********/
		
//		SmsManager sms = SmsManager.getDefault();
//
//		ArrayList<String> msgArray = sms.divideMessage(message);
//
//		int numParts = msgArray.size();
//		ArrayList<PendingIntent> sentPI = new ArrayList<PendingIntent>();
//		ArrayList<PendingIntent> deliveredPI = new ArrayList<PendingIntent>();
//
//		for (int i = 0; i < numParts; i++) {
//			sentPI.add(PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0));
//			deliveredPI.add(PendingIntent.getBroadcast(this, 0, new Intent(
//					DELIVERED), 0));
//		}

		/*********** BroadCasts ***************/
		sendBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS sent",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(getBaseContext(), "Generic failure",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(getBaseContext(), "No service",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(getBaseContext(), "Null PDU",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(getBaseContext(), "Radio off",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		};

		// ---when the SMS has been delivered---
		deliveryBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS delivered",
							Toast.LENGTH_SHORT).show();
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(getBaseContext(), "SMS not delivered",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		};

		registerReceiver(deliveryBroadcastReceiver, new IntentFilter(DELIVERED));
		registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));
//		sms.sendMultipartTextMessage(phoneNumber, null, msgArray, sentPI,
//				deliveredPI);
	}
	@Override
	public void onDestroy() {
		/******
		 * Did you forget to 
		 * unregister your receiver?
		 * ******/
		
		unregisterReceiver(sendBroadcastReceiver);
		unregisterReceiver(deliveryBroadcastReceiver);
		Log.i("Main","onDestroy");
		super.onDestroy();
	}

}
